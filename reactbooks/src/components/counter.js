import React from "react";

import { useDispatch,useSelector } from "react-redux";
import { addbooks, buybooks } from "../action";
const counter = ()=>{
    const count = useSelector((state)=>state.counter.count);
    const dispatch = useDispatch();
    return (
        <div>
            <h1>BOOKSLIST:{count}</h1><br/><br/>
            <button className="btn" onClick={()=> dispatch(addbooks())}>Addbooks</button> <br/><br/>
            
            <button className="btn1" onClick={()=> dispatch(buybooks())}>Buybooks</button>
        </div>
    );
};
export default counter;