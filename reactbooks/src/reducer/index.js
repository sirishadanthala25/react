import { combineReducers } from "redux";
import counterReducer from "./counterreducer";
const rootReducer = combineReducers({
    counter: counterReducer,
});
export default rootReducer;